<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122190653 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE document_sub_group (document_id INT NOT NULL, sub_group_id INT NOT NULL, INDEX IDX_93BC08DDC33F7837 (document_id), INDEX IDX_93BC08DD44FB371E (sub_group_id), PRIMARY KEY(document_id, sub_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE document_sub_group ADD CONSTRAINT FK_93BC08DDC33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_sub_group ADD CONSTRAINT FK_93BC08DD44FB371E FOREIGN KEY (sub_group_id) REFERENCES sub_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE document_sub_group');
    }
}
