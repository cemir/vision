<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221010145555 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE announce CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE certificate CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE complaint CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE criminal CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE folder CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE gang CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE infringement CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE jail CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE medical CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE notification CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE report CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE sanction CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stolenvehicle CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE template CHANGE content content LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE announce CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE certificate CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE comment CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE complaint CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE criminal CHANGE content content TEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE folder CHANGE content content TEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE gang CHANGE content content TEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE infringement CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE jail CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE medical CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE notification CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE report CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE sanction CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE stolenvehicle CHANGE content content TEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE template CHANGE content content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
