<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220120194634 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory ADD gang_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE directory ADD CONSTRAINT FK_467844DA9266B5E FOREIGN KEY (gang_id) REFERENCES gang (id)');
        $this->addSql('CREATE INDEX IDX_467844DA9266B5E ON directory (gang_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory DROP FOREIGN KEY FK_467844DA9266B5E');
        $this->addSql('DROP INDEX IDX_467844DA9266B5E ON directory');
        $this->addSql('ALTER TABLE directory DROP gang_id');
    }
}
