function Raptor() {
    $(
        "<embed src='/sounds/raptor-sound.mp3' hidden='true' autostart='true' loop='false' class='playSound'>" +
            "<audio autoplay='autoplay' style='display:none;' controls='controls'><source src='/sounds/raptor-sound.mp3' /><source src='/sounds/raptor-sound.ogg' /></audio>"
    ).appendTo("body");
    $("body").append('<img id="raptorkonami" style="display: none; z-index: 8000" src="/img/raptor.png" />');
    var jumper = $("#raptorkonami").css({ position: "fixed", bottom: "-700px", right: "0", display: "block" });
    jumper.animate({ bottom: "0" }, 800, function () {
        $(this).animate({ bottom: "-130px" }, 800, function () {
            var offset = $(this).position().left + 400;
            $(this)
                .delay(300)
                .animate({ right: offset }, 2200, function () {
                    jumper = $("#raptorkonami").css({ bottom: "-700px", right: "0" });
                    locked = !1;
                });
        });
    });
}

(function() {
    tinymce.init({
        selector: 'textarea',
        width: '100%',
        //height: 600,        
        skin: 'oxide-dark',
        content_css: 'dark',
        language: 'fr_FR',
        plugins: 'advlist autolink autosave code fullscreen hr image link lists paste quickbars searchreplace table template wordcount',
        menubar: 'edit insert view format table tools link',
        setup: function(ed) {
            ed.on('init', function(e) { ed.save(); });
            ed.on('change', function(e) { ed.save(); });
        },
        toolbar: 'undo redo | bold italic underline | formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor removeformat | link image template | fullscreen | wordcount',
        toolbar_mode: 'wrap',
        browser_spellcheck: true,
        contextmenu: false,
        quickbars_insert_toolbar: false,
        relative_urls: false,
        remove_script_host: true,
        //images_upload_url: '/media/upload',
        //images_upload_credentials: true,
        templates: '/template/json/list',
      });
    
})();

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

var toastElList = [].slice.call(document.querySelectorAll('.toast'))
var toastList = toastElList.map(function (toastEl) {
    return new bootstrap.Toast(toastEl).show()
})

$( document ).ready(function() {
    $('.alertmodal').modal('show');

    $('.dropdown-menu').on('click', function(e) {
        e.stopPropagation();
      });

    $('.mark-notification-readed').click(function(e) {
        let clickedLink = $(this);
        clickedLink.html('<i class="fas fa-spinner fa-pulse"></i>')
        e.preventDefault();        
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),            
            success: function() {
                clickedLink.parent().parent().fadeOut()
            }
        });
    });

    
    var k = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65],
        n = 0;
    $(document).keydown(function (e) {
        if (e.keyCode === k[n++]) {
            if (n === k.length) {
                Raptor();
                n = 0;
                return !1;
            }
        } else n = 0;
    });

});