<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testSomething(): void
    {
        $user = new User();
        $user->setEmail('test@test.com');
        $user->setPassword(uniqid().uniqid().uniqid());
        $user->setFirstname('firstname');
        $user->setLastname('lastname');
        $user->setPhone('123456');


        $this->assertTrue($user->getEmail() === 'test@test.com');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getPhone() === '123456');
        $this->assertTrue($user->getAdminMode() === false);
        $this->assertTrue($user->getIsDesactivated() === false);
        $this->assertTrue($user->getRoles() === ['ROLE_USER']);
    }
}
