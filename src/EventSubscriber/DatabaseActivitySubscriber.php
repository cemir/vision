<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Entity\Comment;
use App\Entity\Notification;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DatabaseActivitySubscriber
{
    protected TokenStorageInterface $tokenStorage;
    protected EntityManagerInterface $entityManager;
    protected Environment $twig;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        Environment $twig
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->notify('postPersist', $args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args): void
    {
        $this->notify('postRemove', $args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->notify('postUpdate', $args);
    }

    private function notify(string $action, LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (
            is_subclass_of($entity, 'App\Entity\Document')
            && method_exists($entity, 'getCreator')
            && in_array($action, ['postUpdate', 'postRemove'])
        ) {
            $sender = $this->tokenStorage->getToken()->getUser();
            $receiver = $entity->getCreator();
            if ($receiver != $sender) {
                $htmlContents = $this->twig->render('_notifications/document_' . $action . '.html.twig', [
                    'sender' => $sender,
                    'document' => $entity
                ]);


                $notification = new Notification(
                    $receiver,
                    $htmlContents,
                    $sender,
                    'notification_icon_document_' . $action
                );

                 $this->entityManager->persist($notification);
                try {
                    $this->entityManager->flush();
                } catch (\Throwable $th) {
                        throw $th;
                }
            }
        }

        if (
            $entity instanceof Comment
            && method_exists($entity, 'getDocument')
            && in_array($action, ['postUpdate', 'postRemove', 'postPersist'])
        ) {
            $sender = $this->tokenStorage->getToken()->getUser();
            $receiver = $entity->getDocument()->getCreator();

            if ($receiver != $sender) {
                $htmlContents = $this->twig->render('_notifications/comment_' . $action . '.html.twig', [
                    'sender' => $sender,
                    'document' => $entity->getDocument()
                ]);


                $notification = new Notification(
                    $receiver,
                    $htmlContents,
                    $sender,
                    'notification_icon_comment_' . $action
                );

                 $this->entityManager->persist($notification);
                try {
                    $this->entityManager->flush();
                } catch (\Throwable $th) {
                        throw $th;
                }
            }
        }
    }
}
