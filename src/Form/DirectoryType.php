<?php

namespace App\Form;

use App\Entity\Directory;
use App\Form\Type\PhoneType;
use App\Form\Type\GenderType;
use App\Form\Type\HeightType;
use App\Form\Type\WeightType;
use App\Form\Type\AddressType;
use App\Form\Type\ContentType;
use App\Form\Type\GangListType;
use App\Form\Type\LastnameType;
use App\Form\Type\FirstnameType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class DirectoryType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /**
         * @var User $user
         */
        $user = $this->security->getUser();
        $userPermissions = array_map('strtolower', $user->getMainRank()->getPermissions());

        $builder
            ->add('firstname', FirstnameType::class)
            ->add('lastname', LastnameType::class)
            ->add('gender', GenderType::class)
            ->add(
                'birthdate',
                BirthdayType::class,
                [
                    'label' => 'form_label_birthday',
                    'placeholder' => [
                                        'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                    ],
                    'required' => false
                    ]
            )
            ->add('phone', PhoneType::class)
            ->add('height', HeightType::class)
            ->add('weight', WeightType::class)
            ->add('address', AddressType::class, ['required' => false])
            ->add('hasNoPapers', null, ['label' => 'form_label_hasnopapers'])
            ;


        if ($user->getAdminMode() || in_array('general_legal_view', $userPermissions)) {
            $builder
            ->add('gang', GangListType::class)
            ->add('gangInfo', null, ['label' => 'form_label_gang_info'])
            ->add('wanted', null, ['label' => 'form_label_wanted'])
            ->add('wantedReason', null, ['label' => 'form_label_wantedReason', 'help' => 'form_help_wantedReason'])
            ->add('wantedDisplay', null, ['label' => 'form_label_wantedDisplay'])
            ->add('wantedPublicDisplay', null, ['label' => 'form_label_wantedPublicDisplay'])
            ->add('wantedPublicReason', null, ['label' => 'form_label_wantedPublicReason'])
            ;
        }

        if ($user->getAdminMode() || in_array('general_medical_view', $userPermissions)) {
            $builder
            ->add('dead', null, ['label' => 'form_label_dead', 'help' => 'form_help_wanted'])
            ->add(
                'medicalContact',
                ContentType::class,
                ['label' => 'form_label_medical_contact',
                'help' => 'form_help_medical_contact',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalLastWish',
                ContentType::class,
                ['label' => 'form_label_medical_lastwish',
                'help' => 'form_help_medical_lastwish',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalTrusted',
                ContentType::class,
                ['label' => 'form_label_medical_trusted',
                'help' => 'form_help_medical_trusted',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalFamilyHistory',
                ContentType::class,
                ['label' => 'form_label_medical_family_history',
                'help' => 'form_help_medical_family_history',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalHistory',
                ContentType::class,
                ['label' => 'form_label_medical_history', 'help' => 'form_help_medical_history',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalAllergies',
                ContentType::class,
                ['label' => 'form_label_medical_allergies', 'help' => 'form_help_medical_allergies',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalBloodGroup',
                null,
                ['label' => 'form_label_medical_blood_group', 'help' => 'form_help_medical_blood_group',
                'required' => false
                ]
            )
            ->add(
                'medicalDrugs',
                ContentType::class,
                ['label' => 'form_label_medical_drugs', 'help' => 'form_help_medical_drugs',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalAlcohol',
                ContentType::class,
                ['label' => 'form_label_medical_alcohol', 'help' => 'form_help_medical_alcohol',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            )
            ->add(
                'medicalTreatment',
                ContentType::class,
                ['label' => 'form_label_medical_treatment','help' => 'form_help_medical_treatment',
                'required' => false,
                'attr' => ['style' => 'height: 400px;']
                ]
            );
        }



        if ($user->getAdminMode() || in_array('general_legal_view', $userPermissions)) {
            $builder->add('faceImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_facepicture'
            ])
            ->add('backImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_backpicture'
            ])
            ->add('leftImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_leftpicture'
            ])
            ->add('rightImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_rightpicture'
            ]);
        }

        $builder


            ->add('idCardImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_idcard'
            ])
            ->add('carLicenceImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_carlicence'
            ])
            ->add('motorcycleLicenceImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_motorcyclelicence'
            ])
            ->add('truckLicenceImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_trucklicence'
            ])
            ->add('boatLicenceImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
                'label' => 'form_label_upload_boatlicence'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_submit',
                'attr' => ['class' => 'btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Directory::class,
        ]);
    }
}
