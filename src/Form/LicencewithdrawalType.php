<?php

namespace App\Form;

use App\Form\DocumentType;
use App\Form\Type\VehicleType;
use App\Entity\Licencewithdrawal;
use App\Form\Type\DateTimeVisionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LicencewithdrawalType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        parent::buildForm($builder, $options);

        $builder

            ->add('type', VehicleType::class)
            ->add('until', null, [
                'label' => 'form_label_until',
                'help' => 'form_help_until',
                'view_timezone' => array_key_exists('TZ', $_ENV) ? $_ENV['TZ'] : false
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Licencewithdrawal::class,
        ]);
    }
}
