<?php

namespace App\Form;

use App\Entity\Jail;
use App\Form\DocumentType;
use App\Form\Type\ContentType;
use App\Form\Type\DateTimeVisionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class JailType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'arrestedAt',
                null,
                [
                    'label' => 'form_label_arrested_at',
                    'help' => 'form_help_arrested_at',
                    'view_timezone' => array_key_exists('TZ', $_ENV) ? $_ENV['TZ'] : false
                ]
            )
            ->add(
                'jailedAt',
                null,
                [
                    'label' => 'form_label_jailed_at',
                    'help' => 'form_help_jailed_at',
                    'view_timezone' => array_key_exists('TZ', $_ENV) ? $_ENV['TZ'] : false
                ]
            )
            ->add('lawyer', CheckboxType::class, ['label' => 'form_label_asked_for_lawyer', 'required' => false])
            ->add('medic', CheckboxType::class, ['label' => 'form_label_asked_for_medic', 'required' => false])
            ->add('content', ContentType::class)
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'form_choice_jail_status_todo' => 'form_choice_jail_status_todo',
                    'form_choice_jail_status_inprogress' => 'form_choice_jail_status_inprogress',
                    'form_choice_jail_status_ended' => 'form_choice_jail_status_ended',
                    'form_choice_jail_status_discontinued' => 'form_choice_jail_status_discontinued',
                ],
                'label' => 'form_label_jail_status',
                'priority' => -800
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Jail::class,
        ]);
    }
}
