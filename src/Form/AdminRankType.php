<?php

namespace App\Form;

use App\Entity\Rank;
use App\Form\Type\PermissionsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminRankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('name', null, ['label' => 'form_label_name'])
            ->add('shortname', null, ['label' => 'form_label_shortname'])
            ->add('power', null, ['label' => 'form_label_power', 'help' => 'form_help_power'])
            ->add('permissions', PermissionsType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_submit',
                'attr' => ['class' => 'btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rank::class,
        ]);
    }
}
