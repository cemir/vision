<?php

namespace App\Form;

use App\Entity\Gang;
use App\Form\DocumentType;
use App\Form\Type\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GangType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        parent::buildForm($builder, $options);
        $builder
        ->add(
            'content',
            ContentType::class,
            ['label' => 'form_label_informations',
            'required' => false,
            ]
        )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Gang::class,
        ]);
    }
}
