<?php

namespace App\Form;

use App\Entity\Criminal;
use App\Form\DocumentType;
use App\Form\Type\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriminalType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        parent::buildForm($builder, $options);

        $builder

            ->add('content', ContentType::class, ['label' => 'form_label_informations' ])
            ->add('amountMoney', null, ['label' => 'form_label_amount', 'help' => 'form_help_amount'])
            ->add('amountTime', null, ['label' => 'form_label_time', 'help' => 'form_help_time'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Criminal::class,
        ]);
    }
}
