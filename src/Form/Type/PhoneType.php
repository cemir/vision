<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class PhoneType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'constraints' => [
                new Regex('/^[0-9 ]*$/', 'form_constraint_digit_only')
            ],
            'attr' => ['placeholder' => 'form_placeholder_phone'],
            'help' => 'form_help_phone',
            'label' => false,
            'required' => false
        ]);
    }

    public function getParent(): string
    {
        return TelType::class;
    }
}
