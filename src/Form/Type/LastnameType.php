<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LastnameType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['placeholder' => 'form_placeholder_lastname'],
            'label' => false
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
