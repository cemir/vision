<?php

namespace App\Form\Type;

use App\PermissionsList;
use Symfony\Component\Form\AbstractType;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PermissionsType extends AbstractType
{
    private CacheInterface $cache;

    public function __construct(CacheInterface $CacheInterface)
    {
        $this->cache = $CacheInterface;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        if ($_ENV['APP_ENV'] === 'dev') {
            $permissionList = new PermissionsList();
            $permissions = $permissionList->getPermissionList();
        } else {
            $permissions = $this->cache->get('vision_permissions', function (ItemInterface $item) {
                $permissionList = new PermissionsList();
                 return $permissionList->getPermissionList();
            });
        }



        $resolver->setDefaults([
            'choices' => $permissions,
            'expanded' => true,
            'multiple' => true,
            'label' => 'form_label_permissions',
            'attr' => ['class' => 'form-switch'],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
