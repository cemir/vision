<?php

namespace App\Form\Type;

use App\Entity\Group;
use App\Repository\GroupRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {

        $resolver->setDefaults([
            'class' => Group::class,
            'query_builder' => function (GroupRepository $GroupRepository) {
                return $GroupRepository->createQueryBuilder('r')
                    ->orderBy('r.name', 'ASC');
            },
            'choice_label' => 'name',
            'placeholder' => 'form_placeholder_group',
            'help' => 'form_help_group',
            'label' => false,
            'required' => true
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
