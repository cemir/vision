<?php

namespace App\Form\Type;

use App\Entity\Group;
use App\Repository\GroupRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AllowedGroupsType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => Group::class,
                'query_builder' => function (GroupRepository $GroupRepository) {
                    return $GroupRepository->createQueryBuilder('r')
                        ->orderBy('r.name', 'ASC');
                },
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'label' => 'form_label_allowedgroups',
                'help' => 'form_help_allowedgroups',
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
