<?php

namespace App\Form;

use App\Form\DocumentType;
use App\Entity\Infringement;
use App\Form\Type\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfringementType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('content', ContentType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Infringement::class,
        ]);
    }
}
