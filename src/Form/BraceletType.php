<?php

namespace App\Form;

use App\Entity\Bracelet;
use App\Form\DocumentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BraceletType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('removingDate', null, [
                'label' => 'form_label_removing_date',
                'view_timezone' => array_key_exists('TZ', $_ENV) ? $_ENV['TZ'] : false
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Bracelet::class,
        ]);
    }
}
