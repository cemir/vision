<?php

namespace App\Entity;

use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CriminalRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CriminalRepository::class)
 */
class Criminal extends Document
{
    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="criminals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountMoney;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountTime;

    /**
     * @ORM\Column(type="text", length=4294967295, nullable=true)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    public function getAmountMoney(): ?float
    {
        return $this->amountMoney;
    }

    public function setAmountMoney(?float $amountMoney): self
    {
        $this->amountMoney = $amountMoney;

        return $this;
    }

    public function getAmountTime(): ?int
    {
        return $this->amountTime;
    }

    public function setAmountTime(?int $amountTime): self
    {
        $this->amountTime = $amountTime;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
