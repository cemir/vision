<?php

namespace App\Entity;

use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\JailRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=JailRepository::class)
 */
class Jail extends Document
{
    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Versioned
     */
    private $arrestedAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Versioned
     */
    private $jailedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="jails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    /**
     * @ORM\Column(type="text", length=4294967295)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $lawyer = false;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $medic = false;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $status;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
        $this->setArrestedAt(new DateTimeImmutable());
        $this->setJailedAt(new DateTimeImmutable());
    }

    public function getArrestedAt(): ?\DateTimeImmutable
    {
        return $this->arrestedAt;
    }

    public function setArrestedAt(\DateTimeImmutable $arrestedAt): self
    {
        $this->arrestedAt = $arrestedAt;

        return $this;
    }

    public function getJailedAt(): ?\DateTimeImmutable
    {
        return $this->jailedAt;
    }

    public function setJailedAt(\DateTimeImmutable $jailedAt): self
    {
        $this->jailedAt = $jailedAt;

        return $this;
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLawyer(): ?bool
    {
        return $this->lawyer;
    }

    public function setLawyer(bool $lawyer): self
    {
        $this->lawyer = $lawyer;

        return $this;
    }

    public function getMedic(): ?bool
    {
        return $this->medic;
    }

    public function setMedic(bool $medic): self
    {
        $this->medic = $medic;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }
}
