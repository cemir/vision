<?php

namespace App\Entity;

use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\FolderRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FolderRepository::class)
 */
class Folder extends Document
{
    /**
     * @ORM\Column(type="text", length=4294967295, nullable=true)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    /**
     * @ORM\ManyToMany(targetEntity=Directory::class, inversedBy="folders")
     */
    private $directories;

    /**
     * @ORM\ManyToMany(targetEntity=Document::class, inversedBy="folders")
     */
    private $documents;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->directories = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Directory[]
     */
    public function getDirectories(): Collection
    {
        return $this->directories;
    }

    public function addDirectory(Directory $directory): self
    {
        if (!$this->directories->contains($directory)) {
            $this->directories[] = $directory;
        }

        return $this;
    }

    public function removeDirectory(Directory $directory): self
    {
        $this->directories->removeElement($directory);

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        $this->documents->removeElement($document);

        return $this;
    }
}
