<?php

namespace App\Entity;

use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TemplateRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TemplateRepository::class)
 */
class Template extends Document
{
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    /**
     * @ORM\Column(type="text", length=4294967295)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;


    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
