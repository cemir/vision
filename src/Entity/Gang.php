<?php

namespace App\Entity;

use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\GangRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=GangRepository::class)
 */
class Gang extends Document
{
    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
        $this->setIsPublic(true);
        $this->directories = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="text", length=4294967295, nullable=true)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity=Directory::class, mappedBy="gang")
     */
    private $directories;

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Directory[]
     */
    public function getDirectories(): Collection
    {
        return $this->directories;
    }

    public function addDirectory(Directory $directory): self
    {
        if (!$this->directories->contains($directory)) {
            $this->directories[] = $directory;
            $directory->setGang($this);
        }

        return $this;
    }

    public function removeDirectory(Directory $directory): self
    {
        if ($this->directories->removeElement($directory)) {
            // set the owning side to null (unless already changed)
            if ($directory->getGang() === $this) {
                $directory->setGang(null);
            }
        }

        return $this;
    }
}
