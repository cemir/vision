<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GroupRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 * @Vich\Uploadable
 * @Gedmo\Loggable
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $name;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="mainGroup")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="mainGroup")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Rank::class, mappedBy="mainGroup")
     */
    private $ranks;

    /**
     * @ORM\ManyToMany(targetEntity=Document::class, mappedBy="allowedGroups")
     */
    private $allowedDocuments;

    /**
     * @ORM\Column(type="string", length=12)
     * @Gedmo\Versioned
     */
    private $shortName;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="mainGroup")
     */
    private $comments;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $motd;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Gedmo\Timestampable(on="change", field={"motd"})
     */
    private $motdUpdatedAt;

    /**
     * @Vich\UploadableField(mapping="group_logo", fileNameProperty="imageName", size="imageSize")
     *
     * @var File|null
     *
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=SubGroup::class, mappedBy="mainGroup", orphanRemoval=true)
     */
    private $subGroups;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->ranks = new ArrayCollection();
        $this->allowedDocuments = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->subGroups = new ArrayCollection();
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setMainGroup($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getMainGroup() === $this) {
                $document->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsersActive(): Collection
    {
        return $this->users->filter(function (User $user) {
            return !$user->getIsDesactivated();
        });
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setMainGroup($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getMainGroup() === $this) {
                $user->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rank[]
     */
    public function getRanks(): Collection
    {
        return $this->ranks;
    }

    public function addRank(Rank $rank): self
    {
        if (!$this->ranks->contains($rank)) {
            $this->ranks[] = $rank;
            $rank->setMainGroup($this);
        }

        return $this;
    }

    public function removeRank(Rank $rank): self
    {
        if ($this->ranks->removeElement($rank)) {
            // set the owning side to null (unless already changed)
            if ($rank->getMainGroup() === $this) {
                $rank->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getAllowedDocuments(): Collection
    {
        return $this->allowedDocuments;
    }

    public function addAllowedDocument(Document $allowedDocument): self
    {
        if (!$this->allowedDocuments->contains($allowedDocument)) {
            $this->allowedDocuments[] = $allowedDocument;
            $allowedDocument->addAllowedGroup($this);
        }

        return $this;
    }

    public function removeAllowedDocument(Document $allowedDocument): self
    {
        if ($this->allowedDocuments->removeElement($allowedDocument)) {
            $allowedDocument->removeAllowedGroup($this);
        }

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(?string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setMainGroup($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getMainGroup() === $this) {
                $comment->setMainGroup(null);
            }
        }

        return $this;
    }

    public function getMotd(): ?string
    {
        return $this->motd;
    }

    public function setMotd(?string $motd): self
    {
        $this->motd = $motd;

        return $this;
    }

    public function getMotdUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->motdUpdatedAt;
    }

    public function setMotdUpdatedAt(?\DateTimeImmutable $motdUpdatedAt): self
    {
        $this->motdUpdatedAt = $motdUpdatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|SubGroup[]
     */
    public function getSubGroups(): Collection
    {
        return $this->subGroups;
    }

    public function addSubGroup(SubGroup $subGroup): self
    {
        if (!$this->subGroups->contains($subGroup)) {
            $this->subGroups[] = $subGroup;
            $subGroup->setMainGroup($this);
        }

        return $this;
    }

    public function removeSubGroup(SubGroup $subGroup): self
    {
        if ($this->subGroups->removeElement($subGroup)) {
            // set the owning side to null (unless already changed)
            if ($subGroup->getMainGroup() === $this) {
                $subGroup->setMainGroup(null);
            }
        }

        return $this;
    }
}
