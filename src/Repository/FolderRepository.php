<?php

namespace App\Repository;

use App\Entity\Folder;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Folder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Folder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Folder[]    findAll()
 * @method Folder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FolderRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Folder::class);
    }
}
