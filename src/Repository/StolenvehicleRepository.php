<?php

namespace App\Repository;

use App\Entity\Stolenvehicle;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Stolenvehicle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stolenvehicle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stolenvehicle[]    findAll()
 * @method Stolenvehicle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StolenvehicleRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stolenvehicle::class);
        $this->fields = ['type', 'numberplate','model', 'content', 'color']; //with title, list fields we can search in
    }
}
