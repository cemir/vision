<?php

namespace App\Repository;

use App\Entity\Jail;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Jail|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jail|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jail[]    findAll()
 * @method Jail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JailRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jail::class);
        $this->fields = ['content']; //with title, list fields we can search in
    }
}
