<?php

namespace App\Repository;

use App\Entity\Criminal;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Criminal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Criminal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Criminal[]    findAll()
 * @method Criminal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CriminalRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Criminal::class);
        $this->fields = ['amountMoney', 'amountTime', 'content']; //with title, list fields we can search in
    }
}
