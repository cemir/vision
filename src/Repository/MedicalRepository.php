<?php

namespace App\Repository;

use App\Entity\Medical;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Medical|null find($id, $lockMode = null, $lockVersion = null)
 * @method Medical|null findOneBy(array $criteria, array $orderBy = null)
 * @method Medical[]    findAll()
 * @method Medical[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicalRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Medical::class);
        $this->fields = ['content']; //with title, list fields we can search in
    }
}
