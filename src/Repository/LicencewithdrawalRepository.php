<?php

namespace App\Repository;

use App\Entity\Licencewithdrawal;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Licencewithdrawal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Licencewithdrawal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Licencewithdrawal[]    findAll()
 * @method Licencewithdrawal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LicencewithdrawalRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Licencewithdrawal::class);
        $this->fields = ['type']; //with title, list fields we can search in
    }
}
