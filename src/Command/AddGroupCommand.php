<?php

namespace App\Command;

use App\Entity\Group;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'vision:addgroup', description: 'Add a group to vision')]
class AddGroupCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('name', InputArgument::OPTIONAL, 'Name of the group')
            ->addArgument('shortname', InputArgument::OPTIONAL, 'Shortname of the group')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $groupName = $input->getArgument('name');
        if (!$groupName) {
            $questionName = new Question('Please enter the name of the group : ', '');
            $questionName->setValidator(function ($answer) {
                if (!is_string($answer) || empty($answer)) {
                    throw new \RuntimeException(
                        'The name c\'ant be empty'
                    );
                }

                return $answer;
            });
            $groupName = $helper->ask($input, $output, $questionName);
        }

        $groupShortName = $input->getArgument('shortname');
        if (!$groupShortName) {
            $questionShortName = new Question('Please enter the short name of the group : ', '');
            $questionShortName->setValidator(function ($answer) {
                if (!is_string($answer) || empty($answer)) {
                    throw new \RuntimeException(
                        'The shortname c\'ant be empty'
                    );
                }

                    return $answer;
            });
            $groupShortName = $helper->ask($input, $output, $questionShortName);
        }

        $Group = new Group();
        $Group->setName($groupName);
        $Group->setShortName($groupShortName);
        $this->entityManager->persist($Group);

        try {
            $this->entityManager->flush();
            $io->success('Group ' . $Group->getName()  . ' created');
            return Command::SUCCESS ;
        } catch (\Throwable $th) {
            $io->error('Error while creating ' . $Group->getName()  . ' group');
            return Command::FAILURE;
        }
    }
}
