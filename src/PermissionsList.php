<?php

namespace App;

class PermissionsList
{
    private const VIEW = 'view';
    private const CREATE = 'create';
    private const EDIT = 'edit';
    private const MERGE = 'merge';
    private const ARCHIVE = 'archive';
    private const DELETE = 'delete';
    private const GROUP = 'group';
    private const OTHERGROUP = 'othergroup';
    private const MEDICAL = 'medical';
    private const LEGAL = 'legal';
    private $permissionsNames;
    private $customPermissions;
    public function __construct()
    {
        $this->permissionsNames = [
                'report',
                'certificate',
                'announce',
                'medical',
                'stolenvehicle',
                'jail',
                'infringement',
                'bracelet',
                'criminal',
                'complaint',
                'licencewithdrawal',
                'template',
                'sanction',
                'gang',
                'folder'
            ];

        $this->customPermissions = [
            'general' => [
                'permission_access_medical' => self::MEDICAL . '_' . self::VIEW,
                'permission_access_legal' => self::LEGAL . '_' . self::VIEW,
            ],
            'group' =>  [
                'permission_administrate_group' => 'administrate',
                'permission_edit_motd' => 'motd',
                'permission_fire_employee' => 'fire',
                'permission_edit_employee' => 'employee',
                'permission_edit_employee_rank' => 'rank',
                'permission_sanction_employee' => 'sanction',
                'permission_ignore_subgroups' => 'ignore_subgroups',
            ],
            'directory' =>  [
                'permission_create_directory' => self::CREATE,
                'permission_edit_directory' => self::EDIT,
                'permission_edit_medical_directory' => self::EDIT . '_' . self::MEDICAL,
                'permission_merge_directories' => self::MERGE,
                'permission_delete_directory' => self::DELETE,
            ],
            'comment' =>  [
                'permission_edit_own_comment' => self::EDIT,
                'permission_edit_own_group_comment' => self::EDIT . '_' . self::GROUP,
                'permission_edit_other_group_comment' => self::EDIT . '_' . self::OTHERGROUP,
                'permission_delete_own_comment' => self::DELETE,
                'permission_delete_own_group_comment' => self::DELETE . '_' . self::GROUP,
                'permission_delete_other_group_comment' => self::DELETE . '_' . self::OTHERGROUP,
                            ]
        ];
    }

    private function buildList()
    {

        $buildedPermissions = null;
        foreach ($this->permissionsNames as $key => $functionName) {
            $buildedPermissions ['permission_title_' . $functionName] = [
                'permission_create_' . $functionName
                    => $functionName . '_' . self::CREATE,
                'permission_edit_own_' . $functionName
                    => $functionName . '_' . self::EDIT,
                'permission_edit_own_group_' . $functionName
                    => $functionName . '_' . self::EDIT . '_' . self::GROUP,
                'permission_edit_other_group_' . $functionName
                    => $functionName . '_' . self::EDIT . '_' . self::OTHERGROUP,
                'permission_archive_own_' . $functionName
                    => $functionName . '_' . self::ARCHIVE,
                'permission_archive_own_group_' . $functionName
                    => $functionName . '_' . self::ARCHIVE . '_' . self::GROUP,
                'permission_archive_other_group_' . $functionName
                    => $functionName . '_' . self::ARCHIVE . '_' . self::OTHERGROUP,
                'permission_delete_own_' . $functionName
                    => $functionName . '_' . self::DELETE,
                'permission_delete_own_group_' . $functionName
                    => $functionName . '_' . self::DELETE . '_' . self::GROUP,
                'permission_delete_other_group_' . $functionName
                    => $functionName . '_' . self::DELETE . '_' . self::OTHERGROUP,
            ];
        }

        foreach ($this->customPermissions as $functionName => $permissions) {
            foreach ($permissions as $desc => $perm) {
                $buildedPermissions['permission_title_' . $functionName][$desc] = $functionName . '_' . $perm;
            }
        }



        foreach ($buildedPermissions as $permCategory => $rows) {
            foreach ($rows as $permName => $permValue) {
                $buildedPermissions[$permCategory][$permName] = strtolower($permValue);
            }
        }

        return $buildedPermissions;
    }

    public function getPermissionList()
    {
        return $this->buildList();
    }
}
