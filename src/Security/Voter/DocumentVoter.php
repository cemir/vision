<?php

namespace App\Security\Voter;

use App\Security\Voter\Tools\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class DocumentVoter extends VoterInterface
{
    protected function supports(string $attribute, $subject): bool
    {
            return in_array($attribute, ['view', 'create', 'edit', 'delete', 'archive'])
            && is_subclass_of($subject, 'App\Entity\Document');
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {

        //first, check if user is valid and has permissions
        if (!$this->checkUser($token->getUser())) {
            return false;
        }

        //if user is in admin mode, bypass
        if ($this->user->getAdminMode()) {
            return true;
        }

        //before all, check if user has specials perms
        //reset prefix, maybe voter has been used before
        $this->setPermissionsPrefix(null);

        if ($subject->getNeedGroupAdministration() && !$this->hasPermission('group_administrate')) {
            return false;
        }

        //then set prefix
        $this->setPermissionsPrefix($subject->getClassShort());

        //Check upon attribute
        switch ($attribute) {
            case 'view':
                return $this->canView($subject);
                break;
            case 'create':
                return $this->canCreate($subject);
                break;

            case 'edit':
                //if document is archived, cannot edit
                if ($subject->getArchive()) {
                    return false;
                }
                return $this->canEdit($subject);
                break;

            case 'delete':
                //if document is archived, cannot delete
                if ($subject->getArchive()) {
                    return false;
                }
                return $this->canDelete($subject);
                break;
            case 'archive':
                return $this->canArchive($subject);
                break;
        }

        return false;
    }

    private function canView($subject)
    {
        //Document is public
        if ($subject->getIsPublic()) {
            return true;
        }

        //Document belong to user main group
        if ($subject->getMainGroup() === $this->user->getMainGroup()) {
            //Document has a restricted subGroup, is the user member of this subgroup ? or maybe he has a bypass perm ?
            if (!$subject->getAllowedSubGroups()->isEmpty() && !$this->hasPermission('group_ignore_subgroups', true)) {
                return $subject->getAllowedSubGroups()
                ->exists(function ($key, $value) {
                    return $this->user->getSubGroups()->contains($value);
                });
            }

            return true;
        }

        //Document not belong to user main group, is the user main group allowed ?
        //if the document as a subGroup, user can't have the subgroup of an other group
        if ($subject->getMainGroup() !== $this->user->getMainGroup() && $subject->getAllowedSubGroups()->isEmpty()) {
            return $subject->getAllowedGroups()->contains($this->user->getMainGroup());
        }

        return false; //false by default
    }

    private function canCreate()
    {
        return $this->hasPermission('CREATE');
    }

    private function canEdit($subject)
    {

        //Document belong to user main group
        if ($subject->getMainGroup() === $this->user->getMainGroup()) {
            if ($subject->getCreator() === $this->user) { //c'est son propre document
                return $this->hasPermission('EDIT');
            }

            return $this->hasPermission('EDIT_GROUP');
        }

        //Document not belong to user main group
        if ($subject->getMainGroup() !== $this->user->getMainGroup()) {
            if ($subject->getAllowedGroups()->contains($this->user->getMainGroup())) {
                return $this->hasPermission('EDIT_OTHERGROUP');
            }
            return false;
        }

        return false; //false by default
    }

    private function canDelete($subject)
    {
        //Document belong to user main group
        if ($subject->getMainGroup() === $this->user->getMainGroup()) {
            if ($subject->getCreator() === $this->user) { //document belong to user
                return $this->hasPermission('DELETE');
            }

            return $this->hasPermission('DELETE_GROUP');
        }

        //Document not belong to user main group
        if ($subject->getMainGroup() !== $this->user->getMainGroup()) {
            if ($subject->getAllowedGroups()->contains($this->user->getMainGroup())) {
                return $this->hasPermission('DELETE_OTHERGROUP');
            }
            return false;
        }

        return false; //false by default
    }

    private function canArchive($subject)
    {
        //Document belong to user main group
        if ($subject->getMainGroup() === $this->user->getMainGroup()) {
            if ($subject->getCreator() === $this->user) { //document belong to user
                return $this->hasPermission('ARCHIVE');
            }

            return $this->hasPermission('ARCHIVE_GROUP');
        }

        //Document not belong to user main group
        if ($subject->getMainGroup() !== $this->user->getMainGroup()) {
            if ($subject->getAllowedGroups()->contains($this->user->getMainGroup())) {
                return $this->hasPermission('ARCHIVE_OTHERGROUP');
            }
            return false;
        }

        return false; //false by default
    }
}
