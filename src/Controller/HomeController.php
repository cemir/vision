<?php

namespace App\Controller;

use App\Repository\DocumentRepository;
use App\Repository\DirectoryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(
        DocumentRepository $DocumentRepository,
        DirectoryRepository $DirectoryRepository
    ): Response {

        return $this->render('home/home.html.twig', [
            'documents' =>
                $DocumentRepository ->listForUser($this->getUser())
                                    ->limit(10)
                                    ->order(['createdAt' => 'DESC'])
                                    ->getResult(),
            'announces' =>
                $DocumentRepository ->listForUser($this->getUser())
                                    ->limitType('announce')
                                    ->limit(3)
                                    ->order(['createdAt' => 'DESC'])
                                    ->getResult(),
            'wanted' =>
                $DirectoryRepository->list()->notDead()->wanted()->wantedDisplay()->getResult(),
            'controller_name' => 'HomeController',
        ]);
    }
}
