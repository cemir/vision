<?php

namespace App\Controller;

use App\Entity\Document;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ShareController extends AbstractController
{
    #[Route('/share/{share}', name: 'share')]
    public function share(Document $Document): Response
    {

        if (!$Document->getAllowShare()) {
            throw new AccessDeniedHttpException('granted_not_allowed_viewing_document');
        }

        return $this->render('document/view.html.twig', [
            'controller_name' => 'DocumentController',
            'document' => $Document,
            'shared' => true
        ]);
    }
}
