<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Document;
use App\Entity\Directory;
use App\Form\SearchBarType;
use App\Repository\DocumentRepository;
use App\Repository\DirectoryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/folder/{id}', name: 'folder_')]
class FolderController extends AbstractController
{
    #[Route('/directory', name: 'directory')]
    #[Route('/directory/wanted', name: 'directory_wanted')]
    #[Route('/directory/dead', name: 'directory_dead')]
    public function directory(
        PaginatorInterface $paginator,
        Request $request,
        Folder $Folder,
        DirectoryRepository $DirectoryRepository
    ): Response {

        $searchForm = $this->createForm(SearchBarType::class);
        $searchForm->handleRequest($request);

        $req = $DirectoryRepository->list()
                        ->search(
                            (
                                $searchForm->isSubmitted()
                                && $searchForm->isValid()
                                && $searchForm->getData()['subject'] !== null
                            ) ? $searchForm->getData()['subject'] : null
                        );

        if ($request->attributes->get('_route') == 'folder_directory_wanted') {
            $req->wanted();
        }

        if ($request->attributes->get('_route') == 'folder_directory_dead') {
            $req->dead();
        } else {
            $req->notDead();
        }

        $pagination = $paginator->paginate(
            $req->getResult(),
            $request->query->getInt('page', 1)
        );


        return $this->render('folder/directory.html.twig', [
            'controller_name' => 'FolderController',
            'folder' => $Folder,
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView(),
            'wanted' => ($request->attributes->get('_route') == 'folder_directory_wanted'),
            'dead' => ($request->attributes->get('_route') == 'folder_directory_dead')
        ]);
    }

    #[Route('/directory/add/{Directory}', name: 'directory_add')]
    public function directoryAdd(
        Request $request,
        Folder $Folder,
        Directory $Directory
    ): Response {
            $Folder->addDirectory($Directory);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Folder);
        try {
            $entityManager->flush();
        } catch (\Throwable $th) {
            if ($_ENV['APP_ENV'] === 'dev') {
                throw $th; //DEBUG
            }

            $this->addFlash('danger', 'alert_error_adding_to_folder');
            return $this->redirectToRoute($request->getRequestUri());
        }

            $this->addFlash('success', 'alert_success_adding_to_folder');
            return $this->redirectToRoute('document_view', ['id' => $Folder->getId()]);
    }

    #[Route('/directory/remove/{Directory}', name: 'directory_remove')]
    public function directoryRemove(
        Request $request,
        Folder $Folder,
        Directory $Directory
    ): Response {
            $Folder->removeDirectory($Directory);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Folder);
        try {
            $entityManager->flush();
        } catch (\Throwable $th) {
            if ($_ENV['APP_ENV'] === 'dev') {
                throw $th; //DEBUG
            }

            $this->addFlash('danger', 'alert_error_removing_from_folder');
            return $this->redirectToRoute($request->getRequestUri());
        }

            $this->addFlash('success', 'alert_success_removing_from_folder');
            return $this->redirectToRoute('document_view', ['id' => $Folder->getId()]);
    }

    // Documents
    #[Route('/document', name: 'document')]
    #[Route('/document/archive', name: 'document_archive')]
    public function document(
        PaginatorInterface $paginator,
        Request $request,
        Folder $Folder,
        DocumentRepository $DocumentRepository
    ): Response {

        $searchForm = $this->createForm(SearchBarType::class);
        $searchForm->handleRequest($request);

        $req = $DocumentRepository->list()
                        ->search(
                            (
                                $searchForm->isSubmitted()
                                && $searchForm->isValid()
                                && $searchForm->getData()['subject'] !== null
                            ) ? $searchForm->getData()['subject'] : null
                        );

        if ($request->attributes->get('_route') == 'folder_document_archive') {
            $req->archive();
        }

        $pagination = $paginator->paginate(
            $req->getResult(),
            $request->query->getInt('page', 1)
        );

        return $this->render('folder/document.html.twig', [
            'controller_name' => 'FolderController',
            'folder' => $Folder,
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView(),
            'archive' => ($request->attributes->get('_route') == 'folder_document_archive'),
        ]);
    }

    #[Route('/document/add/{Document}', name: 'document_add')]
    public function documentAdd(
        Request $request,
        Folder $Folder,
        Document $Document
    ): Response {
            $Folder->addDocument($Document);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Folder);
        try {
            $entityManager->flush();
        } catch (\Throwable $th) {
            if ($_ENV['APP_ENV'] === 'dev') {
                throw $th; //DEBUG
            }

            $this->addFlash('danger', 'alert_error_adding_to_folder');
            return $this->redirectToRoute($request->getRequestUri());
        }

            $this->addFlash('success', 'alert_success_adding_to_folder');
            return $this->redirectToRoute('document_view', ['id' => $Folder->getId()]);
    }

    #[Route('/document/remove/{Document}', name: 'document_remove')]
    public function documentRemove(
        Request $request,
        Folder $Folder,
        Document $Document
    ): Response {
            $Folder->removeDocument($Document);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Folder);
        try {
            $entityManager->flush();
        } catch (\Throwable $th) {
            if ($_ENV['APP_ENV'] === 'dev') {
                throw $th; //DEBUG
            }

            $this->addFlash('danger', 'alert_error_removing_from_folder');
            return $this->redirectToRoute($request->getRequestUri());
        }

            $this->addFlash('success', 'alert_success_removing_from_folder');
            return $this->redirectToRoute('document_view', ['id' => $Folder->getId()]);
    }
}
