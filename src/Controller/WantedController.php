<?php

namespace App\Controller;

use App\Repository\DirectoryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WantedController extends AbstractController
{
    #[Route('/wanted', name: 'app_wanted')]
    public function index(DirectoryRepository $DirectoryRepository): Response
    {
        return $this->render('wanted/index.html.twig', [
            'controller_name' => 'WantedController',
            'wanted' =>
                $DirectoryRepository->list()->notDead()->wanted()->wantedPublicDisplay()->getResult(),
            'shared' => true
        ]);
    }
}
